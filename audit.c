#define _GNU_SOURCE
#include <link.h>
#include <stdio.h>
#include "framework.h"
#include <string.h>
#include <alloca.h>
#include <sys/stat.h>
#include <limits.h>
#include <sys/param.h>


unsigned int la_version(unsigned int version)
{
  return version;
}

char *la_objsearch(const char *name, uintptr_t *cookie,
                          unsigned int flag)
{
char *cwd = malloc(1024);
  char *framework_subst_seq = "${framework}";
  size_t subst_seq_size = strlen(framework_subst_seq);
#if 1 
  fprintf(stderr, "la_objsearch(): name = %s; cookie = %x", name, cookie);
#endif
  int framework = 0;
  int i = 0;
  size_t name_size = strlen(name);
  struct stat stat_buf;


  framework = strncmp(framework_subst_seq, name, sizeof(framework_subst_seq));
  char *buf;

  if (framework == 0)
    {
      char *framework_dir;
      for(i = 0; (framework_dir = framework_dir_n(i)) != NULL; i++)
	{
	  int bufsize = 0;
	  size_t framework_path_size = strlen(framework_dir);

 	  buf = (char *)malloc((name_size + framework_path_size + 1 - subst_seq_size));
	  strncpy(buf, framework_dir, framework_path_size);
	  strncpy(&buf[framework_path_size], &name[subst_seq_size],
		  name_size - subst_seq_size);

	  buf[framework_path_size+name_size] = '\0';

	  if (stat(buf,&stat_buf) == 0)
	    {
	      printf("found %s\n", buf);
	      return buf;
	    } 
	}
    }

  return (char *)name;
}

