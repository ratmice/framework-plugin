# framework plugin for gcc.

## Consists of:
* gcc plugin
* ld.so audit plugin.
* example usage

## gcc plugin
```
Adds support for the Mac OS -framework header search,
and shared library search mechanism.

The resulting binaries will require usage of the ld audit plugin.
```

## ld.so audit library.
```
This will consume an elf substitution sequence
${framework} in DT_NEEDED entries.
converting them to a Mac OS framework style search path.
```

## caveats
```
Needs to be updated to at least the c++ symbol mangling 

To use you need to specify the LD_AUDIT environment variable.
glibc support for loading LD_AUDIT libraries using DT_AUDIT and DT_DEPAUDIT
is not afaik supported.

See the makefile for example usage.
```