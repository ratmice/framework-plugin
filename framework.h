#ifndef framework_plugin_H_
#define framework_plugin_H_

void
add_system_framework_path (char *path);

size_t num_framework_dirs();
char * framework_dir_n(int i);
#endif
