GCC=gcc
CFLAGS=

PLUGIN_SRC_FILES= framework.c plugin.c
PLUGIN_OBJ_FILES= $(patsubst %.c,%.o,$(PLUGIN_SRC_FILES))
PLUGIN_TARGET=framework.so
PLUGIN_LOAD=-fplugin=./$(PLUGIN_TARGET)

TEST_FRAMEWORK_TARGET=test.framework/libtest.so
TEST_FRAMEWORK_OBJ=hello.o

TEST_EXEC=./test-exec

GCCPLUGINS_DIR:= $(shell $(GCC) -print-file-name=plugin)

CFLAGS+= -fPIC
CFLAGS+= -MD 
CFLAGS+= -O2
CFLAGS+= -g
CFLAGS+= -I$(GCCPLUGINS_DIR)/include

AUDIT_TARGET=framework-audit.so
AUDIT_OBJS=audit.o framework.o


.PHONY: clean check all test-libfoo test-test-exec test-test-exec2

all: $(PLUGIN_TARGET) $(TEST_FRAMEWORK_TARGET) $(AUDIT_TARGET) test-exec test-exec2 

$(PLUGIN_TARGET): $(PLUGIN_OBJ_FILES)
	$(GCC) -shared $^ -o $@

check: test-libfoo test-test-exec test-test-exec2

test-libfoo:
	readelf -d $(TEST_FRAMEWORK_TARGET)| grep SONAME

test-test-exec: $(TEST_EXEC)
	readelf -d test-exec | egrep -e '(NEEDED|AUDIT)'
	LD_DEBUG=files LD_AUDIT=./$(AUDIT_TARGET) ./test-exec 2>&1 | grep framework | grep -v audit
	LD_DEBUG=files LD_AUDIT=./$(AUDIT_TARGET) ldd ./test-exec 2>&1 | grep framework | grep -v audit

test-test-exec2: $(TEST_EXEC)
	readelf -d test-exec2 | egrep -e '(NEEDED|AUDIT)'
	LD_DEBUG=files LD_AUDIT=./$(AUDIT_TARGET) ./test-exec2 2>&1 | grep framework | grep -v audit
	LD_DEBUG=files LD_AUDIT=./$(AUDIT_TARGET) ldd ./test-exec2 2>&1 | grep framework | grep -v audit

${TEST_FRAMEWORK_TARGET}: hello.o
	gcc -shared -o $@ $^ -Wl,-soname='$${framework}'/${TEST_FRAMEWORK_TARGET}

$(TEST_FRAMEWORK_OBJ):
	gcc $(CFLAGS) $(PLUGIN_LOAD) -c hello.c -o hello.o

test-exec2: 
	gcc -DDLOPEN $(PLUGIN_LOAD) -o $@ test-exec.c -ldl

test-exec: 
	gcc -v -H $(PLUGIN_LOAD) -o test-exec test-exec.c $(TEST_FRAMEWORK_TARGET)

test-exec.o:
	gcc $(PLUGIN_LOAD) -c $(TEST_EXEC) test-exec.c -o $@

test-exec2.o: 
	gcc $(PLUGIN_LOAD) -DDLOPEN -c test-exec.c -o $@


${AUDIT_TARGET}: $(AUDIT_OBJS) 
	gcc -shared -o $@ $^

clean:
	rm -f $(PLUGIN_OBJ_FILES) $(PLUGIN_TARGET) $(TEST_FRAMEWORK_TARGET) test-exec foo.o test-exec.o framework-audit.so $(AUDIT_OBJS) $(TEST_FRAMEWORK_OBJS) test-exec2 test-exec2.o *.d

-include %.d
