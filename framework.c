/* Darwin support needed only by C/C++ frontends.
   Copyright (C) 2001, 2003, 2004, 2005, 2007, 2008
   Free Software Foundation, Inc.
   Contributed by Apple Computer Inc.

most of This file is from GCC originally in gcc/gcc/config/darwin-c.c

GCC is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3, or (at your option)
any later version.

GCC is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GCC; see the file COPYING3.  If not see
<http://www.gnu.org/licenses/>.  */


#include "config.h"
#include "system.h"
#include "coretypes.h"
#include "cpplib.h"
#include "incpath.h"


static int num_frameworks = 0;
static int max_frameworks = 0;
static bool using_frameworks = false;

static struct frameworks_in_use {
  size_t len;
  const char *name;
  cpp_dir* dir;
} *frameworks_in_use;

struct framework_header {
  const char * dirName;
  int dirNameLen;
};



static struct framework_header framework_header_dirs[] = {
  { "Headers", 7 },
  { "PrivateHeaders", 14 },
  { NULL, 0 }
};


static struct cpp_dir*
find_framework (const char *name, size_t len)
{
  int i;
  for (i = 0; i < num_frameworks; ++i)
    {
      if (len == frameworks_in_use[i].len
          && strncmp (name, frameworks_in_use[i].name, len) == 0)
        {
          return frameworks_in_use[i].dir;
        }
    }
  return 0;
}

/* Remember which frameworks have been seen, so that we can ensure
   that all uses of that framework come from the same framework.  DIR
   is the place where the named framework NAME, which is of length
   LEN, was found.  We copy the directory name from NAME, as it will be
   freed by others.  */

static void
add_framework (const char *name, size_t len, cpp_dir *dir)
{
  char *dir_name;
  int i;
  for (i = 0; i < num_frameworks; ++i)
    {
      if (len == frameworks_in_use[i].len
          && strncmp (name, frameworks_in_use[i].name, len) == 0)
        {
          return;
        }
    }
  if (i >= max_frameworks)
    {
      max_frameworks = i*2;
      max_frameworks += i == 0;
      frameworks_in_use = XRESIZEVEC (struct frameworks_in_use,
                                      frameworks_in_use, max_frameworks);
    }
  dir_name = XNEWVEC (char, len + 1);
  memcpy (dir_name, name, len);
  dir_name[len] = '\0';
  frameworks_in_use[num_frameworks].name = dir_name;
  frameworks_in_use[num_frameworks].len = len;
  frameworks_in_use[num_frameworks].dir = dir;
  ++num_frameworks;
}


static char *
framework_construct_pathname (const char *fname, cpp_dir *dir)
{
  char *buf;
  size_t fname_len, frname_len;
  cpp_dir *fast_dir;
  char *frname;
  struct stat st;
  int i;


  /* Framework names must have a / in them.  */
  buf = strchr (fname, '/');
  if (buf)
    fname_len = buf - fname;
  else
    return 0;

  fast_dir = find_framework (fname, fname_len);

  /* Framework includes must all come from one framework.  */
  if (fast_dir && dir != fast_dir)
    return 0;

  frname = XNEWVEC (char, strlen (fname) + dir->len + 2
                    + strlen(".framework/") + strlen("PrivateHeaders"));
  strncpy (&frname[0], dir->name, dir->len);
  frname_len = dir->len;
  if (frname_len && frname[frname_len-1] != '/')
    frname[frname_len++] = '/';
  strncpy (&frname[frname_len], fname, fname_len);
  frname_len += fname_len;
  strncpy (&frname[frname_len], ".framework/", strlen (".framework/"));
  frname_len += strlen (".framework/");

  if (fast_dir == 0)
    {
      frname[frname_len-1] = 0;
      if (stat (frname, &st) == 0)
        {
          /* As soon as we find the first instance of the framework,
             we stop and never use any later instance of that
             framework.  */
          add_framework (fname, fname_len, dir);
        }
      else
        {
          /* If we can't find the parent directory, no point looking
             further.  */
          free (frname);
          return 0;
        }
      frname[frname_len-1] = '/';
    }

  /* Append framework_header_dirs and header file name */
  for (i = 0; framework_header_dirs[i].dirName; i++)
    {
      strncpy (&frname[frname_len],
               framework_header_dirs[i].dirName,
               framework_header_dirs[i].dirNameLen);
      strcpy (&frname[frname_len + framework_header_dirs[i].dirNameLen],
              &fname[fname_len]);

      if (stat (frname, &st) == 0)
	{
          return frname;
	}
    }

  free (frname);
  return 0;
}

/* Add PATH to the system includes. PATH must be malloc-ed and
   NUL-terminated.  System framework paths are C++ aware.  */
void
add_system_framework_path (char *path)
{
  int cxx_aware = 1;
  cpp_dir *p;

  p = XNEW (cpp_dir);
  p->next = NULL;
  p->name = path;
  p->sysp = 1 + !cxx_aware;
  p->construct = framework_construct_pathname;
  using_frameworks = 1;

  add_cpp_dir_path (p, SYSTEM);
}


const char *framework_defaults [] =
  {
    "/System/Library/Frameworks",
    "/Library/Frameworks",
    ".",
    NULL
  };

size_t num_framework_dirs()
{
  return sizeof framework_defaults / sizeof framework_defaults[0];
}

const char * framework_dir_n(int i)
{
  if (i < sizeof framework_defaults / sizeof framework_defaults[0])
    return framework_defaults[i];
  return NULL;
}
