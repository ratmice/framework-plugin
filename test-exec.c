#include <test/hello.h>
#ifdef DLOPEN
#include <dlfcn.h>
#endif

int main()
{
#ifdef DLOPEN
  dlopen("${framework}/test.framework/libtest.so", RTLD_LAZY);
#else
  hello();
#endif
  return 0;
}
